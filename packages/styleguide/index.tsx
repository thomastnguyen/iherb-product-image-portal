import * as React from "react";
import { render } from "react-dom";

import { Button } from "@iherb/components";

const root = document.getElementById("root");

render(<Button>hello there</Button>, root);
