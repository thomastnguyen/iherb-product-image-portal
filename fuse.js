const { FuseBox, WebIndexPlugin } = require("fuse-box");

const fuse = FuseBox.init({
  homeDir: "packages",
  output: "dist/$name.js",
  target: "browser",
  sourceMaps: true,
  plugins: [
    WebIndexPlugin({
      template: "packages/styleguide/index.html",
    }),
  ],
  alias: {
    "@iherb": "~/",
  },
});

fuse.dev({ open: true });

const app = fuse.bundle("app").instructions(`> styleguide/index.tsx`);

app.hmr().watch();

return fuse.run();
